"use strict";

function showAlert(text) {
    // alert(text)
    console.log(text)
}


function resetForm() {
    document.getElementById("firstname").value = ""
    document.getElementById('lastname').value = ""
    document.getElementById('email').value = ""
    document.getElementById('password').value = ""
    document.getElementById('confirmpassword').value = ""

    var inputs = document.querySelectorAll('.form-check-input');
    for (var i = 0; i < inputs.length; i++) {
        inputs[i].checked = false;
    }
    var summaryElement = document.getElementById("summary");
    summaryElement.setAttribute("hidden", "true");


}

function getRadiovalue() {
    document.getElementById("email").value = document.querySelector('input[name=StudentStatus]:checked').value;
    return document.querySelector('input[name=StudentStatus]:checked').value;
}

function showSummary() {
    var name = document.getElementById("firstname").value
    var lastname = document.getElementById("lastname").value
    var email = document.getElementById("email").value
    var password = document.getElementById('password').value
    var summaryElement = document.getElementById("summary");
    var radio = getRadiovalue();
    summaryElement.innerHTML = "<h3>Form value</h3> <p>Name : " + name + " </p>" +
        "<p>Lastname : " + lastname + "</p>" +
        "<p>Email : " + email + "</p>" +
        "<p>Password : " + password + "</p>" +
        "<p>Status : " + radio + "</p>";
    summaryElement.removeAttribute("hidden")

}

function isEmpty() {
    var name = document.getElementById("firstname").value
    var lastname = document.getElementById("lastname").value
    var email = document.getElementById("email").value
    var password = document.getElementById('password').value

    if (name == "" && lastname == "" && email == "" && password == "") {
        var summaryElement = document.getElementById("summary");
        summaryElement.setAttribute("hidden", "true");
    }
}

function addPElement() {
    var node = document.createElement('p')
    node.innerHTML = 'Hello world'
    var placeHolder = document.getElementById('placeholder')
    placeHolder.appendChild(node)

}

function addHref() {
    var node = document.createElement('p')
    node.innerHTML = 'google'
    node.setAttribute('href', 'https://www.google.com')
    node.setAttribute('target', '_blank')
    var placeHolder = document.getElementById('placeholder')
    placeHolder.appendChild(node)

}

function addBoth() {
    var node = document.createElement('p')
    node.innerHTML = 'Hello world'
    var node2 = document.createElement('a')
    node2.innerHTML = 'google'
    node2.setAttribute('href', 'https://www.google.com')
    node2.setAttribute('target', '_blank')
    var placeHolder = document.getElementById('placeholder')
    placeHolder.appendChild(node)
    placeHolder.appendChild(node2)
}

function addForm() {
    var header = document.createElement('p')
    header.innerHTML = "Plase fill this form to creat account"

    var row1 = document.createElement('div')
    row1.classList.add("row")

    var insiderow1 = document.createElement('div')
    insiderow1.classList.add('col-6')
    var input = document.createElement("INPUT");
    input.setAttribute("type", "text");
    input.classList.add('form-control')
    input.setAttribute('placeholder', 'Firstname')
    insiderow1.appendChild(input)

    var insiderow2 = document.createElement('div')
    insiderow2.classList.add('col-6')
    var input2 = document.createElement("INPUT");
    input2.setAttribute("type", "text");
    input2.classList.add('form-control')
    input2.setAttribute('placeholder', 'Lastname ')
    insiderow2.appendChild(input2)

    var row2 = document.createElement('div')
    row2.classList.add("row")
    var insiderow3 = document.createElement('div')
    insiderow3.classList.add('col')
    var input3 = document.createElement("INPUT");
    input3.setAttribute("type", "email");
    input3.classList.add('form-control')
    input3.setAttribute('placeholder', 'Email ')
    insiderow3.appendChild(input3)
    row2.appendChild(insiderow3)

    row1.appendChild(insiderow1)
    row1.appendChild(insiderow2)

    document.getElementById('addForm').appendChild(header)
    document.getElementById('addForm').appendChild(row1)
    document.getElementById('addForm').appendChild(row2)
}

function creatTable() {
    var tHead = [
        '#', 'First', 'Last', 'handle'
    ]

    var data = [{
            First: 'mark',
            Last: 'Otto',
            Handle: '@mdo'
        },
        {
            First: 'jacob',
            Last: 'Thronton',
            Handle: '@fat'
        },
        {
            First: 'Larry',
            Last: 'the Bird',
            Handle: '@twitter'
        }
    ]

    var table = document.createElement('table')
    table.classList.add('table')
    table.classList.add('table-striped')
    var Thead = document.createElement("thead")
    var Theadtr = document.createElement('tr')

    for (var i = 0; i < tHead.length; i++) {
        var tempHead = document.createElement('th')
        tempHead.setAttribute('scope', 'col')
        tempHead.innerHTML = tHead[i]
        Theadtr.appendChild(tempHead)
    }

    var tBody = document.createElement('tbody')

    for (var i = 0; i < data.length; i++) {
        var rowtemp = document.createElement('tr')
        var rownum = document.createElement('th')
        rownum.setAttribute('scope', 'row')
        rownum.innerHTML = Number(i + 1)

        var dataRowTemp1 = document.createElement('td')
        dataRowTemp1.innerHTML = data[i].First

        var dataRowTemp2 = document.createElement('td')
        dataRowTemp2.innerHTML = data[i].Last

        var dataRowTemp3 = document.createElement('td')
        dataRowTemp3.innerHTML = data[i].Handle

        rowtemp.appendChild(rownum)
        rowtemp.appendChild(dataRowTemp1)
        rowtemp.appendChild(dataRowTemp2)
        rowtemp.appendChild(dataRowTemp3)

        tBody.appendChild(rowtemp)
    }
    table.appendChild(tBody)
    Thead.appendChild(Theadtr)
    table.appendChild(Thead)
    document.getElementById('addTable').appendChild(table)

}

function generateTable() {

    var num = document.getElementById('Tablerows').value
    var table = document.createElement('table')
    table.classList.add('table')
    table.classList.add('table-striped')
    var Thead = document.createElement("thead")
    var Theadtr = document.createElement('tr')

    var tempHead = document.createElement('th')
    tempHead.setAttribute('scope', 'col')
    tempHead.innerHTML = "#";
    Theadtr.appendChild(tempHead)
    Thead.appendChild(Theadtr)
    table.appendChild(Thead)
    var tBody = document.createElement('tbody')
    for (var i = 1; i <= num; i++) {
        var rowtemp = document.createElement('tr')
        var rownum = document.createElement('th')
        rownum.setAttribute('scope', 'row')
        rownum.innerHTML = Number(i)
        rowtemp.appendChild(rownum)
        tBody.appendChild(rowtemp);
    }
    table.appendChild(tBody)

    const myNode = document.getElementById("generateTable");
    while (myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }
        document.getElementById('generateTable').appendChild(table)
    

}